A. INTRODUCTION
=======================

Texture changer - a script needed, an interested volunteer needed to make it

I am posting this project because the only person capable to finish it, who I
know, gave up because of being overwhelmed by another tasks. I have a clear
idea what I could do with that script, I made the objects I want to script and
I do not want to give up on my idea, so I am stuck in the middle for months.

This description was made for a friend who seemed to be sure everything
written as requirements is possible, tough there may be a lot of coding,
which may not be for beginner. I would like to see a help from someone who
knows LSL for Second Life and could take this as a voluntary help. We can
meet in SL or in Opensim to test things, so if you are into coding, if
you like challenges, and if you do not mind to help me with a part of
my own project, please join me. The result will become a part of a scripted
object I make and hopefuly you can learn on it more things you like to do.

To be able to help you may need to register here or it can just serve to you as
the initial source of information. Every move forward is welcome and if we use
Git, more poeple can help. For that case you need to clone this project and know
how to do that. If you want me to submit the changes, if you are inworld only,
not on Gitlab, I will post them, but that way I will be the creator. Let's make
the script as public domain for this reason, which is allowing an unlimited use
for anybody for any purpose. Anyone can do anything to this. The same license
will cover any code and files we will create. The exception is the folder
Examples. There are the files I found/will find as publicly accessible which
can inspire us but they may have different licenses. I was explained that
merging them together is not possible and that we may need absolutely different
code to get similar or better functions. Still, what I found I am providing.

B. DESCRIPTION
======================

Summary of what the scripted object will do:
==============================

1) 
==
t allows dropping textures to no-mod object (see the script in prim, would be done this way)

2)
==
No-mod object is resizeable by the user (see the script resize, done this way)

3)
==
Textures dropped are being processed by the script this way:

a)
--
7 faces (if present) are painted by textures with defined names (optional presence of faces, if not present - will be ignored) 

Note: In the example only 2 such scripts are present, not 8 currently.

b)
--
1 face will be optionally (both options should be present):

- cycling all the textures (see the script in prim) which are not defined in the list  "a" - ideal would be the ability to set time interval present for the user, or
- allow to pick the texture from menu (see the script, but ideally containing more than 22 items) Best will be if the textures defined in the list "a" will not be displayed as the option.


Expected actions from the users
=======================

a) mandatory:
--------------
drop at least one texture when holding CTRL (see 'Dropbox' script)

b) optionally, as they wish:
----------------------------

- picking a texture for permanent display by menu when cycling is not activated (see 'Paint on face' scripts)
- start/stop cycling on a defined face and define time interval (see 'Timer' script)
- resizing object by menu (see 'Resize' script)
- rename one or more textures made by user and drop it when holding CTRL (Required name MAY be provided to the users and it MAY allow them to texture some of other faces)

c) only when necessary:
-----------------------
- write some user friendly data to the configuration notecard and drop it to the object when holding CTRL



'Texture my prim' script(s)
===========================

The object provided contains a lot of code which should be included in the solution (at least the functions should be kept).  It means that most of the functions are already present in the object. The solution itself should aggregate the functions, be easily manageable by a common user - see "Expected actions from the users". Some minor functions should be added or extended.

1) more than 22 textures to select from - unlimited?
= SOLVES CURRENT LIMIT IN PROVIDED SCRIPT ('Paint on face ...' scripts)

2) single menu - no more menus appearing on touch (problem with appearing more menus for resizing, more faces
= SOLVES CURRENT PROBLEM OF 3 MENUS APPEARING (there would be now even 9 menus when 8 faces used)

3) switchable automatic cycling (on- rotates, off -does not rotate)
= SOLVES CURRENTLY NOT PRESENT FUNCTION in menu ('Timer' script)

4) texture more faces (8) (only one needs to be rotated), texturing of other faces may be based on the list of assigned  textures provided (see 5)
= SOLVES THE PROBLEM WITH DISPLAYING UNWANTED textures on main face by excluding the names specified on some list. Connected to the need to simplify the menu.

5) define which faces will be painted on (1-8) plus assigning the textures to faces:
= DEFINES FIXED NAMES FOR up to 7 textures on 7 faces plus 1 main face.

a) one face will cycle all textures in content, display them and will ignore only those assigned to other faces (those will not be rotating)
b) names of the ignored textures for other faces, e. g. face1=cyclingtexturename  minus (list of textures for faces 2-8:  face2=logo2, face3=logo3,face4=logo4,face5=logo5,face6=logo6,Face7=logo7,face8=logo8 (Note: missing faces should be ignored, not required)

NOTE: There should be a way how to be sure, that user is NOT allowed to use textures for some faces. Using one script for each face required to be textured by the user may be the solution for no mod objects.

6) All actions should be available only to the owner. Optionally assigned to group or everyone (This is not required but can be handy.)
= FIXES POTENTIONALY UNWANTED ACTIONS performed by public

7) The script should not consume too many resources - many such objects can be on the parcel.

8) Optionally please comment the code a bit when writing it, so it could be understood and be a source for learning lsl.

9) All configuration options would be better on the top of the script to be easily found, optionally in some configuration notecard.


